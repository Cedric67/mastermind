#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <stdbool.h>

using namespace std;

//Structure de base
struct Jeu
{
    int combinaisonOrdi[4];
    int combinaisonJoueur[4];
    int essais=10;
    int gagne=0;
    int perdu=0;
}joueur;

//D�roulement du jeu
int partie()
{
    //Initialisation des variables
    int i=0;
    int gagne=false;
    joueur.essais=10;

    //Initialisation des valeurs Ordi
    for (i=0; i<4; ++i)
    {
        joueur.combinaisonOrdi[i]=rand()%10;
    }

    //Tant que le joueur n'a pas gagne et que le nombre d'essai est diff�rent de 0
    while ((joueur.essais!=0)&&(gagne==false))
    {
        int chiffre=0;
        int combinaisonCorrect=0;
        cout << "Il vous reste " << joueur.essais << " essais." << endl;

        //Le joueur entre ses valeurs
        cout << "Entrez quatre chiffres compris entre 0 et 9 : " << endl;
        for (i=0; i<4; ++i)
        {
            cout << "Chiffre " << i+1 << " : ";
            cin >> chiffre;
            joueur.combinaisonJoueur[i]=chiffre;
            while ((chiffre<0)||(chiffre>9))
            {
                cout << "Entrer un autre chiffre " << i+1 << " : " << endl;
                cin >> chiffre;
                joueur.combinaisonJoueur[i]=chiffre;
            }
        }
        cout << endl;

        //Verifie la combinaison du joueur par rapport � celle de l'ordi
        for (i=0; i<4; ++i)
        {
            if (joueur.combinaisonJoueur[i]==joueur.combinaisonOrdi[i])
            {
                cout << "Votre " << joueur.combinaisonJoueur[i] << " est correct en chiffre " << i+1 << endl;
                combinaisonCorrect+=1;
            }
            else
            {
                for (int j=0; j<4; ++j)
                {
                    if ((joueur.combinaisonJoueur[i]==joueur.combinaisonOrdi[j])&&(joueur.combinaisonJoueur[i]!=joueur.combinaisonJoueur[j]))
                    {
                        cout << "Votre chiffre " << joueur.combinaisonJoueur[i] << " est mal place." << endl;
                    }
                }
            }
        }
        cout << endl;

        //Si les 4 combinaisons sont corrects, le joueur gagne
        if (combinaisonCorrect==4)
        {
            cout << "Wundervoll ! Sie haben gewonnen ! Hurra !" << endl << endl;
            gagne=true;
            return gagne;
        }

        //Si le joueurs n'a pas trouv� toutes les combinaisons
        else
        {
            joueur.essais-=1;
        }

        //Si le joueur perd
        if (joueur.essais==0)
        {
            //Affiche la combinaison gagnante
            cout << "La bonne combinaison etait " << joueur.combinaisonOrdi[0] << " " << joueur.combinaisonOrdi[1] << " " << joueur.combinaisonOrdi[2] << " " << joueur.combinaisonOrdi[3] << endl;;
            return joueur.essais;
        }
    }
}



int main()
{
    //Initialisation des variables
    int i=0;
    int partie();
    int rejouer=1;
    bool gagne=false;

    //randomize
    srand(time(NULL));


    //D�but de partie
    cout << "Bonjour, vous jouez au Mastermind.\nJe vous souhaite bonne chance...\n\nLa partie commence !"<< endl << endl;

    //Tant que le joueur veut rejouer
    while (rejouer==1)
    {

        gagne=partie();

        //Si le joueur gagne
        if (gagne==true)
        {
            joueur.gagne+=1;
        }
        //Si le joueur perd
        else
        {
            joueur.perdu+=1;
        }

        //Affiche le nombre de partie gagnee et perdue
        cout << "Vous avez " << joueur.gagne << " partie gagne et " << joueur.perdu << " partie perdu." << endl;

        //Demande au joueur s'il veut rejouer ou quitter
        cout << "Voulez-vous rejouer ? Entrer 0 pour non, 1 pour oui : ";
        cin >> rejouer;
        while ((rejouer<0)||(rejouer>1))
            {
                cout << "Je n'ai pas compris, entrez 0 ou 1 : " << endl;
                cin >> rejouer;
            }
        cout << endl;
    }

    //The End
    cout << "Merci d'avoir joue :)\nA bientot !" << endl;

    return 0;
}
